import _imports
import pytest
from pybi.utils.helper import style_text2dict


def test_base():
    style_text = "width:20rem;height:20rem;border-color:#00FF00 #00FF00 #00FF00 #00FF00;border-style:outset"

    act = style_text2dict(style_text)

    exp = {
        "width": "20rem",
        "height": "20rem",
        "border-color": "#00FF00 #00FF00 #00FF00 #00FF00",
        "border-style": "outset",
    }

    assert act == exp


def test_multiple_lines():
    style_text = """
    background: rgba(35, 113, 198, 0.8);
    color: aliceblue;
    padding: 0.8em;
    font-size: 1.2em;
    font-family: Arial, Helvetica, sans-serif;
    font-weight: bold;
"""

    act = style_text2dict(style_text)

    exp = {
        "background": "rgba(35, 113, 198, 0.8)",
        "color": "aliceblue",
        "padding": "0.8em",
        "font-size": "1.2em",
        "font-family": "Arial, Helvetica, sans-serif",
        "font-weight": "bold",
    }

    assert act == exp
