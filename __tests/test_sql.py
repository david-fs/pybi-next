import _imports
import pytest
import pybi.utils.sql as sqlUtils
from pybi.core.sql import SqlInfo
import pybi as pbi


def test_extract_table_names_normal():

    act = sqlUtils.extract_table_names("select 类别,名字,值 from test")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_normal_upper():

    act = sqlUtils.extract_table_names("SELECT 类别,名字,值 FROM test")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_with_where():

    act = sqlUtils.extract_table_names("select a from test where a=='from'")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_with_query():

    act = sqlUtils.extract_table_names("select a (select * from test) where a=='from'")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_with_join():

    act = sqlUtils.extract_table_names(
        "select a from tab1 as t1 join tab2 as t2 on t1.key=t2.key  where t1.a=='from'"
    )

    exp = ["tab1", "tab2"]

    assert act == exp


def test_extract_fields_head_select():
    act = sqlUtils.extract_fields_head_select("select a,b,c from (select x from tab1)")

    exp = ["a", "b", "c"]

    assert act == exp


def test_extract_fields_head_select_alias():
    act = sqlUtils.extract_fields_head_select(
        "select a as name1,b,c from (select x from tab1)"
    )

    exp = ["name1", "b", "c"]

    assert act == exp


def test_extract_fields_head_select_func():
    act = sqlUtils.extract_fields_head_select(
        "select a, round(avg(b),2) as fun1,round(avg(c),2) as fun2  from tab"
    )

    exp = ["a", "fun1", "fun2"]

    assert act == exp


def test_extract_fields_head_select_mul_lines():
    act = sqlUtils.extract_fields_head_select(
        """select a, 
        round(avg(b),2) as fun1,
        round(avg(c),2) as fun2  
        from tab"""
    )

    exp = ["a", "fun1", "fun2"]

    assert act == exp


def test_extract_table_names_apply_agg():
    agg = "round(avg(${}),2)"
    y = "销量"

    act = sqlUtils.apply_agg(agg, y)
    exp = "round(avg(销量),2)"

    assert exp == act


def test_extract_table_names_univ_apply_agg():
    agg = "sum"
    y = "销量"

    act = sqlUtils.apply_agg(agg, y)
    exp = f"{agg}(`{y}`)"

    assert exp == act


class Test_extract_sql_from_text(object):
    def assert_sql(self, a: SqlInfo, b: SqlInfo):
        assert a.sql == b.sql and a.type == b.type and a.jsMap == b.jsMap

    def assert_infos_text(self, act, exp):
        assert len(act) == len(exp)
        for a, e in zip(act, exp):
            if isinstance(a, SqlInfo):
                self.assert_sql(a, e)
            else:
                assert a == e

    def test_normal(self):
        sql = pbi.sql("select sum(销售额) from data")
        input = f"总销售额:{sql}"
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = ["总销售额:", sql._sql_info]

        self.assert_infos_text(act, exp)

    def test_only_one_value(self):
        input = f"1"
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = ["1"]

        self.assert_infos_text(act, exp)

    def test_mul_sql(self):
        sql1 = pbi.sql("select sum(销售额) from data")
        sql2 = pbi.sql("select a,b,c from data")

        input = f"总销售额:{sql1} xx{sql2}end"
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = [
            "总销售额:",
            sql1._sql_info,
            " xx",
            sql2._sql_info,
            "end",
        ]

        self.assert_infos_text(act, exp)

    def test_mul_line_sql(self):
        sql1 = pbi.sql(
            """
select sum(销售额) 
from data"""
        )
        sql2 = pbi.sql("select a,b,c from data")

        input = f"总销售额:{sql1} xx{sql2}end"
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = [
            "总销售额:",
            sql1._sql_info,
            " xx",
            sql2._sql_info,
            "end",
        ]

        self.assert_infos_text(act, exp)

    def test_mul_sql_coiled(self):
        sql1 = pbi.sql("select sum(销售额) from data")
        sql2 = pbi.sql("select a,b,c from data")
        input = f"总销售额:{sql1}{sql2}end xx "
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = [
            "总销售额:",
            sql1._sql_info,
            sql2._sql_info,
            "end xx ",
        ]

        self.assert_infos_text(act, exp)

    def test_no_sql_flag(self):
        sql1 = pbi.sql("select sum(销售额) from data")
        input = f"sql语句[select sum(销售额) from data]的结果是 {sql1}"
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = [
            "sql语句[select sum(销售额) from data]的结果是 ",
            sql1._sql_info,
        ]

        self.assert_infos_text(act, exp)

    def test_all_sql(self):
        sql1 = pbi.sql("select sum(销售额) from data")
        input = f"{sql1}"
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = [sql1._sql_info]

        self.assert_infos_text(act, exp)

    def test_html_text(self):
        sql1 = pbi.sql("select sum(销售额) from data")
        input = f"""<h1>title</h1>\n\n<ul>\n<li>task1:{sql1}</li>\n<li>task2</li>\n</ul>\n\n<h2>sub title</h2>\n\n<ol>\n<li>xxx</li>\n<li>zzz</li>\n</ol>\n\n<p><a href="https://www.baidu.com">百度</a></p>\n"""
        act = list(SqlInfo.extract_sql_from_text(input))

        exp = [
            "<h1>title</h1>\n\n<ul>\n<li>task1:",
            sql1._sql_info,
            '</li>\n<li>task2</li>\n</ul>\n\n<h2>sub title</h2>\n\n<ol>\n<li>xxx</li>\n<li>zzz</li>\n</ol>\n\n<p><a href="https://www.baidu.com">百度</a></p>\n',
        ]

        self.assert_infos_text(act, exp)


#
