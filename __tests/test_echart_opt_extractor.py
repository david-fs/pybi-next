import _imports
import pytest
from pybi.core.components.reactiveComponent.echarts import OptionsExtractor
import pybi.utils.sql as sqlUtils
import pybi as pbi


def test_extract_1():

    opts = {"series": [{"data": [1, 2, 3]}, {"data": [{"item": {"value": 10}}]}]}
    OptionsExtractor.set_none_prop(opts, "series[1].data[0].item.value")

    assert opts["series"][1]["data"][0]["item"]["value"] is None


def test_extract_2():

    opts = {"series": [{"data": [1, 2, 3]}, {"data": [{"item": {"value": 10}}]}]}
    OptionsExtractor.set_none_prop(opts, "series")

    assert opts["series"] is None


def test_extract_3():

    opts = {"series": [{"data": [1, 2, 3]}, {"data": [{"item": {"value": 10}}]}]}
    OptionsExtractor.set_none_prop(opts, "series[0]")

    assert opts["series"][0] is None
