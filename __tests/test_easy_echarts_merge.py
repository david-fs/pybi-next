import _imports
import pytest
from pybi.easyEcharts.utils import merge_user_options


def test_with_index():
    base_opts = {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx"},
        ]
    }

    user_opts = {
        "yAxis[0]": {
            "min": 0,
            "max": 180,
        }
    }

    act = merge_user_options(base_opts, user_opts)

    exp = {
        "yAxis": [
            {"data": [1, 2, 3], "min": 0, "max": 180},
            {"name": "xx"},
        ]
    }

    # 保证 base_opts 没有被修改
    assert base_opts == {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx"},
        ]
    }
    assert act == exp


def test_with_index1():
    base_opts = {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx"},
        ]
    }

    user_opts = {
        "yAxis[1]": {
            "min": 0,
            "max": 180,
        }
    }

    act = merge_user_options(base_opts, user_opts)

    exp = {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx", "min": 0, "max": 180},
        ]
    }

    # 保证 base_opts 没有被修改
    assert base_opts == {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx"},
        ]
    }
    assert act == exp


def test_without_index():
    base_opts = {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx"},
        ]
    }

    user_opts = {
        "yAxis": {
            "min": 0,
            "max": 180,
        }
    }

    act = merge_user_options(base_opts, user_opts)

    exp = {
        "yAxis": [
            {"data": [1, 2, 3], "min": 0, "max": 180},
            {"name": "xx"},
        ]
    }

    # 保证 base_opts 没有被修改
    assert base_opts == {
        "yAxis": [
            {"data": [1, 2, 3]},
            {"name": "xx"},
        ]
    }
    assert act == exp


def test_without_index2():
    base_opts = {"yAxis": [{}]}

    user_opts = {
        "yAxis": {
            "min": 0,
            "max": 180,
        }
    }

    act = merge_user_options(base_opts, user_opts)

    exp = {"yAxis": [{"min": 0, "max": 180}]}

    # 保证 base_opts 没有被修改
    assert base_opts == {"yAxis": [{}]}
    assert act == exp


def test_():
    base_opts = {
        "yAxis": [
            {"data": [1, 2, 3], "min": 999, "others": {"a": 1}},
            {"name": "xx"},
        ]
    }

    user_opts = {"yAxis[0]": {"min": 0, "max": 180, "others": {"b": 999}}}

    act = merge_user_options(base_opts, user_opts)

    exp = {
        "yAxis": [
            {"data": [1, 2, 3], "min": 0, "max": 180, "others": {"a": 1, "b": 999}},
            {"name": "xx"},
        ]
    }

    # 保证 base_opts 没有被修改
    assert base_opts == {
        "yAxis": [
            {"data": [1, 2, 3], "min": 999, "others": {"a": 1}},
            {"name": "xx"},
        ]
    }
    assert act == exp
