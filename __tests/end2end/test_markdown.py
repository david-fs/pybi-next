import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = True


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_normal:
    @pytest.fixture
    def file_url(self):
        pbi.add_markdown("---").set_debugTag("hr full width")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        assert utils.err_count(page) == 0

    def test_hr_full_width(self, page: Page):
        md = utils.PageMarkdown(page, "hr full width")

        hr = md.box.locator("> hr")

        hr_width = hr.evaluate("node=>  node.clientWidth")
        parent_dom = hr.locator("../..")
        paddings = parent_dom.evaluate(
            """node => {
            const style = window.getComputedStyle(node);
            pd = style.getPropertyValue('padding-left').replace('px','')
            return parseInt(pd) * 2
        }"""
        )

        hr_width = hr_width + paddings
        body_width = page.evaluate("()=>document.body.clientWidth")
        page.pause()
        assert (hr_width / body_width) > 0.90


class Test_linkage_sql_query:
    @pytest.fixture
    def file_url(self):
        df = pd.DataFrame(
            [
                ["catA", 100],
                ["catB", 100],
            ],
            columns=["cat", "value"],
        )

        data = pbi.set_source(df)

        value_total = pbi.sql(f"select  sum(value)+100 as value from {data}")

        pbi.add_markdown(
            f"""
# title({value_total})
- task1:`{value_total}`
- task2
        """
        ).set_debugTag("md")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        assert utils.err_count(page) == 0

    def test_should_display_num(self, page: Page):
        md = utils.PageMarkdown(page, "md")
        assert md.box.get_by_role("heading").inner_text() == "title(300)"

        items = md.box.get_by_role("listitem").all_text_contents()
        assert items == ["task1:300", "task2"]
