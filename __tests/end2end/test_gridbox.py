import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
from pybi.core.components import Table

import pandas as pd
from pathlib import Path
import utils


m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame({"name": list("abcd"), "value": [1, 2, 3, 4]})

        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        data = pbi.set_source(data_df)

        tables: list[Table] = []
        with pbi.gridBox("").auto_fill_by_num(2):
            for n in range(5):
                db_tag = f"tab_{n}"
                t = pbi.add_table(data).set_style("width:100%").set_debugTag(db_tag)
                tables.append(t)

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()

    def test_auto_fill_by_num(self, page: Page):
        tags = [f"tab_{n}" for n in range(5)]
        tables = [page.locator(f'css= *[data-debug-tag="{tag}"]') for tag in tags]
        rects = [t.evaluate("node => node.getBoundingClientRect()") for t in tables]

        # 通过 y 确定它们的行位置正确
        assert rects[0]["y"] == rects[1]["y"], "t0 and t1 should be on the first line"
        assert rects[2]["y"] == rects[3]["y"], "t2 and t3 should be on the second line"

        assert rects[2]["y"] > (
            rects[0]["y"] + rects[0]["height"]
        ), "The starting y of t2 should be greater than the starting y of t0 plus the height of t0."

        assert rects[4]["y"] > (
            rects[2]["y"] + rects[2]["height"]
        ), "The starting y of t4 should be greater than the starting y of t2 plus the height of t2."

        assert rects[3]["y"] > (
            rects[1]["y"] + rects[2]["height"]
        ), "The starting y of t3 should be greater than the starting y of t1 plus the height of t1."

        # 通过 x确定列位置正确
        assert rects[0]["x"] == rects[2]["x"], "t0 and t2 should be on the same column"
        assert rects[2]["x"] == rects[4]["x"], "t2 and t4 should be on the same column"

        assert rects[1]["x"] > (
            rects[0]["x"] + rects[0]["width"]
        ), "The x of t1 should be greater than the x of t0 plus the width."

        assert rects[3]["x"] > (
            rects[2]["x"] + rects[0]["width"]
        ), "The x of t3 should be greater than the x of t2 plus the width."

        # page.pause()


class Test_areas:
    @pytest.fixture
    def file_url(self):
        grid_layout = """
            t1 t2
            t3 t4
        """

        with pbi.gridBox(grid_layout):
            pbi.add_text("t4").set_gridArea("t4").set_debugTag("t4")
            pbi.add_text("t3").set_gridArea("t3").set_debugTag("t3")
            pbi.add_text("t2").set_gridArea("t2").set_debugTag("t2")
            pbi.add_text("t1").set_gridArea("t1").set_debugTag("t1")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_right_position(self, page: Page):
        tags = [f"t{n}" for n in range(1, 5)]
        texts = [page.locator(f'css= *[data-debug-tag="{tag}"]') for tag in tags]
        rects = [t.evaluate("node => node.getBoundingClientRect()") for t in texts]

        # 通过 y 确定它们的行位置正确
        assert rects[0]["y"] == rects[1]["y"], "t1 and t2 should be on the first line"
        assert rects[2]["y"] == rects[3]["y"], "t3 and t4 should be on the second line"

        assert rects[2]["y"] > (
            rects[0]["y"] + rects[0]["height"]
        ), "The starting y of t2 should be greater than the starting y of t0 plus the height of t0."

        assert rects[3]["y"] > (
            rects[1]["y"] + rects[2]["height"]
        ), "The starting y of t3 should be greater than the starting y of t1 plus the height of t1."

        # 通过 x确定列位置正确
        assert rects[0]["x"] == rects[2]["x"], "t0 and t2 should be on the same column"

        assert rects[1]["x"] > (
            rects[0]["x"] + rects[0]["width"]
        ), "The x of t1 should be greater than the x of t0 plus the width."

        assert rects[3]["x"] > (
            rects[2]["x"] + rects[0]["width"]
        ), "The x of t3 should be greater than the x of t2 plus the width."

        # page.pause()
