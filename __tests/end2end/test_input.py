import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            {"name": ["abc", "a1", "xxab", "yy'y", "zzz"], "value": range(5)}
        )
        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        pbi.preset_ui("quasar")
        file = Path("test_result.html")

        data = pbi.set_source(data_df)

        pbi.add_table(data).set_debugTag("table")

        pbi.add_input(data["name"]).set_debugTag("input-contains")
        pbi.add_input(data["name"], where_expr=' like "${}%" ').set_debugTag(
            "input-start"
        )

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()

    def test_should_contains(self, page: Page):
        table = utils.PageTable(page, "table")

        input_contains = utils.PageInput(page, "input-contains")
        input_contains.input("a")

        assert table.get_table_col_values(0) == ["abc", "a1", "xxab"]
        assert table.get_table_col_values(1) == ["0", "1", "2"]

        input_contains.clear()

        assert table.get_table_col_values(0) == ["abc", "a1", "xxab", "yy'y", "zzz"]
        assert table.get_table_col_values(1) == ["0", "1", "2", "3", "4"]

        input_contains.input("'")
        assert table.get_table_col_values(0) == ["yy'y"]

    def test_should_starts(self, page: Page):
        table = utils.PageTable(page, "table")

        input = utils.PageInput(page, "input-start")
        input.input("a")

        assert table.get_table_col_values(0) == ["abc", "a1"]
        assert table.get_table_col_values(1) == ["0", "1"]

        input.clear()

        assert table.get_table_col_values(0) == ["abc", "a1", "xxab", "yy'y", "zzz"]
        assert table.get_table_col_values(1) == ["0", "1", "2", "3", "4"]
