import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = True


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["n1", 1, 10],
                ["n2", 2, 20],
                ["n3", 3, 30],
                ["n1", 4, 40],
                ["n4", 5, 50],
            ],
            columns=["name", "value1", "value2"],
        )

        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)

        pbi.add_slicer(data["name"]).set_debugTag("name slicer")

        top_value1 = pbi.sql(f"select max(value2) from {data}")
        pbi.add_text(f"test num:{top_value1}").set_debugTag("top value1")

        rows = pbi.sql(f"select * from {data}")
        pbi.add_text(f"row is dict:{rows}").set_debugTag("all rows")

        rows_count = pbi.sql(f"select count(1) from {data}")
        pbi.add_text(f"rows count:{rows_count}").set_debugTag("rows count")

        rows_count = pbi.sql(f"select count(1) from {data}")
        pbi.add_text(f"{rows_count}").set_debugTag("rows count number")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()

    def test_should_(self, page: Page):
        top1_text = utils.PageText(page, "top value1")
        assert top1_text.get_text() == "test num:50"

        all_rows_text = utils.PageText(page, "all rows")
        assert (
            all_rows_text.get_text()
            == """row is dict:[{"name":"n1","value1":1,"value2":10},{"name":"n2","value1":2,"value2":20},{"name":"n3","value1":3,"value2":30},{"name":"n1","value1":4,"value2":40},{"name":"n4","value1":5,"value2":50}]"""
        )

        rows_count_number_text = utils.PageText(page, "rows count number")
        assert rows_count_number_text.get_text() == "5"

        # page.pause()

    def test_sql_query(self, page: Page):
        rows_count_text = utils.PageText(page, "rows count")
        assert rows_count_text.get_text() == "rows count:5"

        name_slicer = utils.PageSlicer(page, "name slicer")
        name_slicer.switch_options_pane()
        name_slicer.select_options_by_text("n1")

        assert rows_count_text.get_text() == "rows count:2"
