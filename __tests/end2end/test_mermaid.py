import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_display:
    @pytest.fixture
    def file_url(self):
        pbi.add_mermaid(
            """
        flowchart LR
            done
        """
        ).set_debugTag("g1")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()

    def test_should_text_done(self, page: Page):
        g1 = utils.PageMermaid(page, "g1")

        g1.should_contain("done")

        # page.pause()


class Test_click_event:
    @pytest.fixture
    def file_url(self):
        mm = pbi.add_mermaid(
            """
        graph TB
        FullFirstSquad-->StripedFirstSquad
        """
        ).set_debugTag("graph")

        mm.add_graph(
            """
            flowchart LR
        A[Hard] -->|Text| B(Round)
        B --> C{Decision}
        C -->|One| D[Result 1]
        C -->|Two| E[Result 2]
        """,
            "g2",
        )

        mm.set_relationship("main.FullFirstSquad>g2")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()

    def test_should_click_and_back(self, page: Page):
        g = utils.PageMermaid(page, "graph")
        g.click("FullFirstSquad")

        g.should_contain("Hard")

        g.hover()
        g.click("返回")

        g.should_contain("FullFirstSquad")
