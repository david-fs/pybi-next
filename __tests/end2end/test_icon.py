import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

md_icons = pbi.md_icons

m_headless = False


@pytest.fixture(scope="module")
def page():

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_display:
    @pytest.fixture
    def file_url(self):

        # pbi.add_svg_icon(
        #     '<svg  viewBox="0 0 24 24"><path fill="currentColor" d="M11 2v20c-5.1-.5-9-4.8-9-10s3.9-9.5 9-10m2 0v9h9c-.5-4.8-4.2-8.5-9-9m0 11v9c4.7-.5 8.5-4.2 9-9h-9Z"/></svg>'
        # )

        # pbi.add_svg_icon(
        #     '<svg  viewBox="0 0 24 24"><path fill="currentColor" d="M11 2v20c-5.1-.5-9-4.8-9-10s3.9-9.5 9-10m2 0v9h9c-.5-4.8-4.2-8.5-9-9m0 11v9c4.7-.5 8.5-4.2 9-9h-9Z"/></svg>',
        #     size="5em",
        #     color="red",
        # )

        # pbi.add_svg_icon(
        #     '<svg  viewBox="0 0 24 24"><path fill="currentColor" d="M11 2v20c-5.1-.5-9-4.8-9-10s3.9-9.5 9-10m2 0v9h9c-.5-4.8-4.2-8.5-9-9m0 11v9c4.7-.5 8.5-4.2 9-9h-9Z"/></svg>',
        #     color="rgb(229, 197, 26)",
        # )

        with pbi.flowBox():
            for icon in [
                md_icons.md_close,
                md_icons.md_open_in_browser,
                md_icons.md_text_snippet,
            ]:
                pbi.add_icon(icon).set_classes("pybi-animate-bounce")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
