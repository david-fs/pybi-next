import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        page.set_default_timeout(2000)
        yield page


class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            {"name": ["abc", "a1", "xxab", "yyy", "zzz"], "value": range(5)}
        )
        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)

        pbi.add_table(data).set_debugTag("table")

        pbi.add_numberSlider(data, "value").set_debugTag("slider")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0

    def test_should_(self, page: Page):
        table = utils.PageTable(page, "table")

        slider = utils.PageNumberSlider(page, "slider")
        slider.drag_start_point("ArrowRight")

        assert table.get_table_col_values(0) == ["a1", "xxab", "yyy", "zzz"]
        assert table.get_table_col_values(1) == ["1", "2", "3", "4"]

        slider.drag_end_point("ArrowLeft")
        assert table.get_table_col_values(0) == ["a1", "xxab", "yyy"]
        assert table.get_table_col_values(1) == ["1", "2", "3"]
