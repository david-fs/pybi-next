import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils
from typing import Dict
from pyecharts import options as opts
from pyecharts.charts import Bar
from pyecharts.commons.utils import JsCode
from pyecharts.globals import ThemeType

m_headless = False

pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def page():

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        page.set_default_timeout(2000)
        yield page


class Test_use_dataset:
    @pytest.fixture
    def file_url(self):

        df = pd.DataFrame(
            [
                [89.3, 58212, "Matcha Latte"],
                [57.1, 78254, "Milk Tea"],
                [74.4, 41032, "Cheese Cocoa"],
                [50.1, 12755, "Cheese Brownie"],
                [89.7, 20145, "Matcha Cocoa"],
                [68.1, 79146, "Tea"],
                [19.6, 91852, "Orange Juice"],
                [10.6, 101852, "Lemon Juice"],
                [32.7, 20112, "Walnut Brownie"],
            ],
            columns=["score", "amount", "product"],
        )

        data = pbi.set_source(df)

        query = pbi.sql(f"select * from {data}")

        c = (
            Bar()
            .add_dataset(source=query.toflatlist(with_header=True))
            .add_yaxis(
                series_name="",
                y_axis=[],
                encode={"x": "amount", "y": "product"},
                label_opts=opts.LabelOpts(is_show=False),
            )
            .set_global_opts(
                title_opts=opts.TitleOpts(title="Dataset normal bar example"),
                xaxis_opts=opts.AxisOpts(name="amount"),
                yaxis_opts=opts.AxisOpts(type_="category"),
                visualmap_opts=opts.VisualMapOpts(
                    orient="horizontal",
                    pos_left="center",
                    min_=10,
                    max_=100,
                    range_text=["High Score", "Low Score"],
                    dimension=0,
                    range_color=["#D7DA8B", "#E15457"],
                ),
            )
        )

        pbi.add_slicer(data["product"]).set_debugTag("product")

        pbi.add_echart(c).set_debugTag("chart")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    def test_should_no_errs(self, page: Page, file_url):
        page.goto(file_url)

        assert utils.err_count(page) == 0

    def test_can_slicer_select(self, page: Page, file_url):
        page.goto(file_url)
        slicer = utils.PageSlicer(page, "product")
        chart = utils.PageEChart(page, "chart")
        opts = chart.get_options()

        assert opts["dataset"][0]["source"] == [
            ["score", "amount", "product"],
            [89.3, 58212, "Matcha Latte"],
            [57.1, 78254, "Milk Tea"],
            [74.4, 41032, "Cheese Cocoa"],
            [50.1, 12755, "Cheese Brownie"],
            [89.7, 20145, "Matcha Cocoa"],
            [68.1, 79146, "Tea"],
            [19.6, 91852, "Orange Juice"],
            [10.6, 101852, "Lemon Juice"],
            [32.7, 20112, "Walnut Brownie"],
        ]
        assert opts["visualMap"][0]["show"] == True
        assert opts["series"][0]["type"] == "bar"
        assert opts["series"][0]["encode"] == {"x": "amount", "y": "product"}

        # slicer changed
        slicer.switch_options_pane()
        slicer.select_options_by_text("Milk Tea")
        slicer.select_options_by_text("Cheese Cocoa")
        assert utils.err_count(page) == 0

        opts = chart.get_options()

        assert opts["dataset"][0]["source"] == [
            ["score", "amount", "product"],
            [57.1, 78254, "Milk Tea"],
            [74.4, 41032, "Cheese Cocoa"],
        ]
        assert opts["visualMap"][0]["show"] == True
        assert opts["series"][0]["type"] == "bar"
        assert opts["series"][0]["encode"] == {"x": "amount", "y": "product"}


class Test_use_jscode:
    @pytest.fixture
    def file_url(self):

        list2 = [
            {"value": 12, "percent": 12 / (12 + 3), "cat": "A"},
            {"value": 23, "percent": 23 / (23 + 21), "cat": "A"},
            {"value": 33, "percent": 33 / (33 + 5), "cat": "A"},
            {"value": 3, "percent": 3 / (3 + 52), "cat": "A"},
            {"value": 33, "percent": 33 / (33 + 43), "cat": "A"},
        ]

        list3 = [
            {"value": 3, "percent": 3 / (12 + 3), "cat": "B"},
            {"value": 21, "percent": 21 / (23 + 21), "cat": "B"},
            {"value": 5, "percent": 5 / (33 + 5), "cat": "B"},
            {"value": 52, "percent": 52 / (3 + 52), "cat": "B"},
            {"value": 43, "percent": 43 / (33 + 43), "cat": "B"},
        ]

        df = pd.DataFrame(list2 + list3)
        data = pbi.set_source(df)
        query = pbi.sql(f"select * from {data}")

        c = (
            Bar(init_opts=opts.InitOpts(theme=ThemeType.LIGHT))
            .add_xaxis([1, 2, 3, 4, 5])
            .add_yaxis(
                "product1",
                pbi.sql(f'select * from {data} where cat="A"'),
                stack="stack1",
                category_gap="50%",
            )
            .add_yaxis(
                "product2",
                pbi.sql(f'select * from {data} where cat="B"'),
                stack="stack1",
                category_gap="50%",
            )
            .set_series_opts(
                label_opts=opts.LabelOpts(
                    position="right",
                    formatter=JsCode(
                        "function(x){return Number(x.data.percent * 100).toFixed() + '%';}"
                    ),
                )
            )
        )
        pbi.add_slicer(data["cat"]).set_debugTag("cat")

        pbi.add_echart(c).set_debugTag("chart")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    def test_should_no_errs(self, page: Page, file_url):
        page.goto(file_url)

        assert utils.err_count(page) == 0

    def test_can_slicer_select(self, page: Page, file_url):
        page.goto(file_url)
        slicer = utils.PageSlicer(page, "cat")
        chart = utils.PageEChart(page, "chart")
        opts = chart.get_options()

        assert len(opts["series"]) == 2
        assert opts["series"][0]["label"]["formatter"] is None
        assert opts["series"][0]["data"] == [
            {"value": 12, "percent": 0.8, "cat": "A"},
            {"value": 23, "percent": 0.5227272727272727, "cat": "A"},
            {"value": 33, "percent": 0.868421052631579, "cat": "A"},
            {"value": 3, "percent": 0.05454545454545454, "cat": "A"},
            {"value": 33, "percent": 0.4342105263157895, "cat": "A"},
        ]
        assert opts["series"][1]["data"] == [
            {"value": 3, "percent": 0.2, "cat": "B"},
            {"value": 21, "percent": 0.4772727272727273, "cat": "B"},
            {"value": 5, "percent": 0.13157894736842105, "cat": "B"},
            {"value": 52, "percent": 0.9454545454545454, "cat": "B"},
            {"value": 43, "percent": 0.5657894736842105, "cat": "B"},
        ]

        # slicer changed
        slicer.switch_options_pane()
        slicer.select_options_by_text("B")
        assert utils.err_count(page) == 0

        opts = chart.get_options()
        assert len(opts["series"]) == 2
