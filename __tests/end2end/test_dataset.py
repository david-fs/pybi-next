import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = True

pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def page():

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_sql:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["广东省", "广州市", "荔湾区"],
                ["广东省", "广州市", "海珠区"],
                ["广东省", "广州市", "白云区"],
                ["广东省", "深圳市", "南山区"],
                ["广东省", "深圳市", "盐田区"],
                ["广东省", "深圳市", "福田区"],
                ["湖南省", "长沙市", "芙蓉区"],
                ["湖南省", "长沙市", "天心区"],
                ["湖南省", "株洲市", "石峰"],
                ["湖南省", "株洲市", "渌口"],
            ],
            columns=list("省市区"),
        )

        df["value"] = range(1, len(df) + 1)
        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):

        file = Path("test_result.html")

        data = pbi.set_source(data_df)
        dv = pbi.set_dataView(f"select 省,sum(value) as total from {data} group by 省")
        pbi.add_table(dv)

        dv = pbi.set_dataView(f"select * from {data}")
        pbi.add_table(dv)

        dv1 = pbi.set_dataView(f"select * from {dv} inner join {data}")
        pbi.add_table(dv1)

        dv1 = pbi.set_dataView(
            f"select * from {dv} as a left join {data} as b on a.省=b.省"
        )
        pbi.add_table(dv1)

        df = pd.DataFrame([[1, 2], [2, 3]])

        data = pbi.set_source(df)
        pbi.add_table(dv1)

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        pass

        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()


class Test_sql_dv_join:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame({"name": list("aabbcd"), "value": [1, 2, 3, 4, 5, 6]})
        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):

        file = Path("test_result.html")

        data = pbi.set_source(data_df)

        dv1 = pbi.set_dataView(
            f"select name,sum(value) as total from {data} group by name"
        )
        dv2 = pbi.set_dataView(f"select sum(value) as alltotal from {data}")

        dv = pbi.set_dataView(f"select total/alltotal from {dv1} inner join {dv2}")

        pbi.add_table(dv)

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        pass

        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()


class Test_linkage:
    @pytest.fixture
    def file_url(self):

        df1 = pd.DataFrame(
            [
                ["catA", 100],
                ["catB", 100],
            ],
            columns=["cat", "value"],
        )

        data1 = pbi.set_source(df1)

        dv1 = pbi.set_dataView(f"select * from {data1}")
        dv2 = pbi.set_dataView(f"select * from {dv1}", [dv1])

        pbi.add_slicer(dv1["cat"]).set_debugTag("cat slicer")
        pbi.add_table(dv2).set_debugTag("table")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        assert utils.err_count(page) == 0

    def test_should_cannot_effect_table(self, page: Page):
        slicer = utils.PageSlicer(page, "cat slicer")
        table = utils.PageTable(page, "table")

        slicer.switch_options_pane()
        slicer.select_options_by_text("catB")

        assert table.get_rows().count() == 2
