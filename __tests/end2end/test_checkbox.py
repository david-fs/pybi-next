import _imports
import pytest
from playwright.sync_api import Page, sync_playwright
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_normal:
    @pytest.fixture
    def init_df(self):
        df = pd.DataFrame(
            [
                ["广东省", "广州市", "荔湾区"],
                ["广东省", "广州市", "海珠区"],
                ["广东省", "广州市", "白云区"],
                ["广东省", "深圳市", "南山区"],
                ["广东省", "深圳市", "盐田区"],
                ["广东省", "深圳市", "福田区"],
                ["湖南省", "长沙市", "芙蓉区"],
                ["湖南省", "长沙市", "天心区"],
                ["湖南省", "株洲市", "石峰"],
                ["湖南省", "株洲市", "渌口"],
            ],
            columns=list("省市区"),
        )

        df["index"] = range(0, len(df))
        df["order1"] = range(len(df) - 1, -1, -1)

        return df

    @pytest.fixture
    def init_has_null_df(self):
        df = pd.DataFrame(
            {
                "name": [None, "X", "Y"],
            }
        )

        return df

    @pytest.fixture
    def file_url(self, init_df, init_has_null_df):
        data = pbi.set_source(init_df)
        data_has_null = pbi.set_source(init_has_null_df)

        for name in "省市区":
            pbi.add_checkbox(data[name]).set_debugTag(name)

        pbi.add_checkbox(data["index"]).set_debugTag("index")
        pbi.add_checkbox(data["index"], orderby="order1").set_debugTag(
            "index_with_order1_asc"
        )
        pbi.add_checkbox(data["index"], orderby="order1 desc").set_debugTag(
            "index_with_order1_desc"
        )

        pbi.add_checkbox(data_has_null["name"]).set_debugTag("has_null")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0

    def test_should_opts_values_省(self, page: Page, init_df):
        check_pvc = utils.PageCheckbox(page, "省")

        assert check_pvc.get_option_values() == init_df["省"].drop_duplicates().tolist()

    def test_should_opts_values_市(self, page: Page, init_df: pd.DataFrame):
        check_city = utils.PageCheckbox(page, "市")

        assert (
            check_city.get_option_values()
            == init_df["市"].drop_duplicates().sort_values().tolist()
        )

    def test_should_opts_values_区(self, page: Page, init_df: pd.DataFrame):
        check_area = utils.PageCheckbox(page, "区")

        assert (
            check_area.get_option_values()
            == init_df["区"].drop_duplicates().sort_values().tolist()
        )

    def test_should_opts_values_linkage_省(self, page: Page, init_df: pd.DataFrame):
        check_pvc = utils.PageCheckbox(page, "省")

        # 广东省
        check_pvc.click_by_text("广东省")

        check_city = utils.PageCheckbox(page, "市")

        assert check_city.get_option_values() == ["广州市", "深圳市"]

        #
        check_area = utils.PageCheckbox(page, "区")

        assert (
            check_area.get_option_values()
            == init_df[lambda x: x["省"] == "广东省"]["区"]
            .drop_duplicates()
            .sort_values()
            .tolist()
        )

    def test_should_orderby_default(self, page: Page, init_df: pd.DataFrame):
        check_index = utils.PageCheckbox(page, "index")
        assert check_index.get_option_values() == list(init_df["index"].astype(str))

    def test_should_orderby_asc(self, page: Page, init_df: pd.DataFrame):
        check_index_order_asc = utils.PageCheckbox(page, "index_with_order1_asc")

        assert check_index_order_asc.get_option_values() == list(
            init_df.sort_values("order1")["index"].astype(str)
        )

    def test_should_orderby_desc(self, page: Page, init_df: pd.DataFrame):
        check_index_order_asc = utils.PageCheckbox(page, "index_with_order1_desc")

        assert check_index_order_asc.get_option_values() == list(
            init_df.sort_values("order1", ascending=False)["index"].astype(str)
        )


class Test_check_mutil_source:
    @pytest.fixture
    def file_url(self):
        df1 = pd.DataFrame(
            [
                ["catA", 100],
                ["catB", 100],
            ],
            columns=["cat", "value"],
        )

        df2 = pd.DataFrame(
            [
                ["catA", 5000],
                ["catB", 6000],
            ],
            columns=["cat", "value"],
        )

        data1 = pbi.set_source(df1)
        data2 = pbi.set_source(df2)

        pbi.add_checkbox(data1["cat"]).add_updateInfo(
            data2.source_name, "cat"
        ).set_debugTag("slicer")

        pbi.add_table(data2).set_debugTag("table")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_cancel_selected(self, page: Page):
        cb = utils.PageCheckbox(page, "slicer")
        table = utils.PageTable(page, "table")

        cb.click_by_text("catB")

        assert table.get_rows().count() == 1
        assert table.get_row_values(0) == ["catB", "6000"]

        # cancle catB selected
        cb.click_by_text("catB")
        assert table.get_rows().count() == 2

    def test_should_reset_selected(self, page: Page):
        cb = utils.PageCheckbox(page, "slicer")
        table = utils.PageTable(page, "table")

        cb.click_by_text("catB")

        assert table.get_rows().count() == 1
        assert table.get_row_values(0) == ["catB", "6000"]

        # reset
        cb.click_reset()
        assert table.get_rows().count() == 2

    def test_should_deSelected(self, page: Page):
        cb = utils.PageCheckbox(page, "slicer")
        table = utils.PageTable(page, "table")

        cb.click_by_text("catB")

        assert table.get_rows().count() == 1
        assert table.get_row_values(0) == ["catB", "6000"]

        # de select
        cb.click_deselect()
        assert table.get_rows().count() == 1
        assert table.get_row_values(0) == ["catA", "5000"]

    def test_should_allSelected(self, page: Page):
        cb = utils.PageCheckbox(page, "slicer")
        table = utils.PageTable(page, "table")

        cb.click_by_text("catB")

        assert table.get_rows().count() == 1
        assert table.get_row_values(0) == ["catB", "6000"]

        # de select
        cb.click__all_select()
        assert table.get_rows().count() == 2
