import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        page = browser.new_page()
        yield page


class Test_FlowBox:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            {
                "产品类别": [
                    "c1",
                    "c2",
                    "c3",
                    "c1",
                    "c2",
                    "c3",
                ],
                "利润额": [150, 230, 224, 218, 135, 147],
                "区域": ["z1", "z1", "z1", "z2", "z2", "z2"],
            }
        )
        return df

    @pytest.fixture
    def file_url(
        self,
        data_df: pd.DataFrame,
    ):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)

        opt = charts.make_bar(data, x="产品类别", y="利润额", color="区域")

        with pbi.flowBox():
            pbi.add_echart(opt).set_debugTag("chart in flowbox")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_no_err(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0

        # page.pause()

    def test_chart_width(self, page: Page):
        chart = utils.PageEChart(page, "chart in flowbox").scroll2show()

        width = chart.chart_box.evaluate("node=> node.clientWidth")
        body_width = page.evaluate("()=>document.body.clientWidth")

        width_percent = width / body_width

        assert width_percent > 0.1
