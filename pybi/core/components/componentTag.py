from enum import Enum


class ComponentTag(Enum):
    App = "App"
    DataSource = "DataSource"
    Slicer = "elSlicer"
    QsSlicer = "qsSlicer"
    Checkbox = "Checkbox"
    Input = "elInput"
    QsInput = "qsInput"
    NumberSlider = "elNumberSlider"
    Table = "elTable"
    QsTable = "qsTable"
    EChart = "EChart"
    Box = "Box"
    Text = "Text"
    Upload = "Upload"
    TextValue = "TextValue"
    Markdown = "Markdown"
    ColBox = "ColBox"
    FlowBox = "FlowBox"
    GridBox = "GridBox"
    Tabs = "elTabs"
    QsTabs = "qsTabs"
    Icon = "Icon"
    SvgIcon = "SvgIcon"
    Affix = "Affix"
    Mermaid = "Mermaid"
    Space = "Space"
    Foreach = "Foreach"
    Sizebar = "elSizebar"
    QsSizebar = "qsSizebar"
    QsButton = "qsButton"
    Img = "img"
