import { type SqlValue } from "sql.js";
import { ComputedRef, ref } from "vue";

type TTableData = ComputedRef<{
  toRowsArrayHasHeader: () => Generator<SqlValue[], void, unknown>;
}>;

export function useCopyData2excel() {
  function copy(tableData: TTableData) {
    const lines = [];
    for (const row of tableData.value.toRowsArrayHasHeader()) {
      lines.push(
        row.map((r) => `"${r?.toString().replaceAll(`"`, `""`)}"`).join("\t")
      );
    }

    const excelData = lines.join("\n");

    navigator.clipboard.writeText(excelData).then(() => {
      Quasar.Notify.create({ message: "copied!", position: "top" });
    });
  }

  return {
    copy,
  };
}
