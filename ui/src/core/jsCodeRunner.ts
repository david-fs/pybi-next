import { TSqlInfoType } from "@/models/reactiveComponent";
import { TQueryAllResult } from "@/services/dbServices"
import { uniq, groupBy } from "lodash-es";


const m_utilsFuncs = {
    uniq,
    groupBy,
}

/**
 * 
 * @param code 
 * @returns 
 */
export function createSqlQueryMappingRunner(code: string | null, type: TSqlInfoType) {
    const mapFunc = new Function('utils', 'rows', 'fields', code ?? '')



    function map(queryResult: TQueryAllResult) {

        switch (type) {

            case 'udf':
                if (code !== null) {
                    return mapFunc(m_utilsFuncs, queryResult.rows, queryResult.fields)
                }
                break;
            case 'infer':
            default:
                return queryResult.inferData()
                break;
        }


    }

    return {
        map
    }
}