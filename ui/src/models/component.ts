import { TSqlInfo } from "./reactiveComponent"
import { Statement } from "./statements"
import { TForeachRow } from "./types"


export enum ComponentTag {
    App = "App",
    DataSource = "DataSource",
    Slicer = "Slicer",
    Markdown = 'Markdown',
    Table = "Table",
    Box = "Box",
    ColBox = "ColBox",
    GridBox = "GridBox",
    FlowBox = "FlowBox",
    EChart = "EChart",
    Upload = 'Upload',
    TextValue = "TextValue",
    Tabs = "Tabs",
    Icon = 'Icon',
    SvgIcon = 'SvgIcon',
    Space = 'Space',
    Foreach = 'Foreach',
    Checkbox = 'Checkbox'
}

const containerTags = new Set<ComponentTag>([
    ComponentTag.App, ComponentTag.Box, ComponentTag.ColBox,
    ComponentTag.FlowBox, ComponentTag.GridBox])

export function isContainer(tag: ComponentTag) {
    return containerTags.has(tag)
}



export interface Component {
    id: string
    tag: ComponentTag
    styles: Record<string, any>
    props: Record<string, any>
    visible?: boolean | TSqlInfo
    gridArea?: string
    debugTag: string | null
    classes: string[]
    foreachRow?: TForeachRow
}


export interface TextComponent extends Component {
    content: string
}

export interface UploadComponent extends Component {

}

// export interface SvgIconComponent extends Component {
//     svg: string
//     size: string
//     color: string
// }

export interface IconComponent extends Component {
    // iconID: string
    // size: string
    // color: string
}

export interface SpaceComponent extends Component {

}


export interface ButtonComponent extends Component {
    action?: {
        id: string
        name: string
    }
}


export interface ImgComponent extends Component {
    src: {
        id: string
    }
}




export interface VisibleColumnsSlicerComponent extends Component {
    tableId: string
}