import { Component } from "./component";
import { TSqlInfo } from "./reactiveComponent";

export interface Container extends Component {
  children: Component[];
}

export interface BoxContainer extends Container {
  align?: string;
  verticalAlign?: string;
}

export interface GridContainer extends Container {
  areas: string;
  gridTemplateColumns: string;
  gridTemplateRows: string;
  align?: string;
  verticalAlign?: string;
}

export interface ColBoxContainer extends Container {
  spec: number[];
}

export interface FlowBoxContainer extends Container {
  align?: string;
}

export interface ForeachContainer extends Container {
  sql: TSqlInfo;
}

export interface SizebarContainer extends Container {}

export interface TabsContainer extends Container {
  names: string[];
  // mode: 'fullWidth' | 'narrowing'
  icons: string[];
  tabsProps: Record<string, any>;
  tabsStyles: Record<string, any>;
  panelsProps: Record<string, any>;
  panelsStyles: Record<string, any>;
  tabsClasses: string[];
  panelsClasses: string[];
  tabPosition?: "left" | "right" | "top" | "bottom";
}
