



export function isSqlInfo(obj: any) {
    return 'sql' in obj && 'type' in obj && 'jsMap' in obj
}