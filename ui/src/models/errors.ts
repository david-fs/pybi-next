


export class AppInitError extends Error {

    /**
     *
     */
    constructor(message: string, public suggestion?: string) {
        super(message);
        this.name = 'AppInitError'
    }

}