import { Component } from "./component"
import { TForeachMapping } from "./types"



export interface ReactiveComponent extends Component {
    linkages: string[]
    sourceName: string | null
    updateInfos: {
        table: string
        field: string
    }[]

}

export type TSqlInfoType = 'udf' | 'infer'

export type TSqlInfo = {
    sql: string
    type: TSqlInfoType
    jsMap: string
}

export interface SingleReactiveComponent extends ReactiveComponent {
    sqlInfo: TSqlInfo
}



export interface Slicer extends SingleReactiveComponent {
    multiple: boolean
    title: string
    field: string
    hiddenTitle?: boolean
    defaultSelected?: boolean
}

export interface Checkbox extends SingleReactiveComponent {
    title: string
    mode: 'tile' | 'list'
    itemDirection: 'row' | 'column'
    multiple: boolean
    field: string
    hiddenTitle?: boolean
}

export interface Input extends ReactiveComponent {
    whereExpr: string
}

export interface NumberSlider extends ReactiveComponent {
    whereExpr: string
    initValue: number[] | number
}


export interface Table extends SingleReactiveComponent {
    pageSize: number
    tableHeight: string
    tableWidth: string
    columnProps?: Record<string, Record<string, any>>
    showCopyButton?: boolean
    topSlot: Component[]
}

export type TUpdateInfoValueType = 'x' | 'y' | 'value' | 'color' | 'name'
export type TUpdateInfoActionType = 'hover' | 'click'


export type TEchartUpdateInfo = {
    actionType: TUpdateInfoActionType
    valueType: TUpdateInfoValueType
    table: string
    field: string
}


export type TEChartType = 'line' | 'pie' | 'bar' | 'scatter' | 'map' | 'radar'
export type TEChartOptions = { series: {}[], yAxis: {}[], xAxis: {}[] }
export type TEChartDatasetInfo = {
    seriesConfig: { type: TEChartType },
    path: string
    sqlInfo: TSqlInfo
}
export type TJsCode = {
    path: string
    code: string
}

export type TEChartInfo = {
    options: TEChartOptions
    mapIds: string[]
    datasetInfos: TEChartDatasetInfo[]
    updateInfos: TEchartUpdateInfo[]
    jsCodes: TJsCode[]
    postMergeSettings: object
}

export interface EChart extends ReactiveComponent {
    width: string
    height: string
    optionType: 'dict' | 'easyChart'
    chartInfos: TEChartInfo[]
}

export function createUpdateId(echart: EChart, updateIndex: number) {
    return `${echart.id}_opt${updateIndex}`
}




export interface TextValue extends SingleReactiveComponent {
    contexts: (string | TSqlInfo | TForeachMapping)[]
}

export interface Markdown extends SingleReactiveComponent {
    contexts: (string | TSqlInfo | TForeachMapping)[]
}