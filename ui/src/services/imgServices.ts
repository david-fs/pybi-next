import { App } from '@/models/app'


export type TImgService = ReturnType<typeof getServices>

export function getServices(app: App) {
    const mapping = new Map<string, string>()

    app.imgResources.forEach(rs => {
        mapping.set(rs.id, rs.bs64)
    })

    function getBs64(id: string) {
        if (!mapping.has(id)) {
            throw new Error(`img Resource[${id}] not found`);
        }

        return mapping.get(id)!
    }

    return {
        getBs64,
    }
}


function fetchAction(args: any) {
    const { data } = useFetch(args.url, args.options).json()
    return data
}

function cacheFn(fn: () => any) {
    let result = null as any

    return () => {
        if (result) {
            return result
        }

        result = fn()
        return result
    }
}

function getDbFileWebResource(wr: WebResource) {
    const getter = cacheFn(() => wr.input)
    return getter
}

function getEChartsMapWebResource(wr: WebResource) {

    if (wr.input) {
        const getter = cacheFn(() => {
            (echarts as any).registerMap(wr.id, { geoJSON: wr.input });
            return ref(true)
        })
        return getter
    }

    const fetchArgs = wr.actionPipe[0]
    const args = fetchArgs.args as {
        url: string
        options: any
    }

    const getter = cacheFn(() => {
        const { isFinished, data } = useFetch(args.url, args.options).get().json()

        watch(data, data => {
            (echarts as any).registerMap(wr.id, { geoJSON: data });
        })

        return isFinished
    })
    return getter
}

export function BuildWebResourceGetter(wr: WebResource) {
    switch (wr.type) {
        case 'DbFile':
            return getDbFileWebResource(wr)
            break;

        case 'echarts-map':
            return getEChartsMapWebResource(wr)
            break;

        default:
            throw new Error("not suport");
            break;
    }
}


