
export type TComponentInfosService = ReturnType<typeof getServices>


export function getServices() {
    const mapping = new Map<string, Record<string, any>>()

    function setInfo(componentId: string, info: Record<string, any>) {
        mapping.set(componentId, info)
    }

    function getInfo<T>(componentId: string) {
        if (!mapping.has(componentId)) {
            throw new Error("组件没有设置 info!");
        }
        return mapping.get(componentId)! as T
    }


    return {
        setInfo,
        getInfo
    }
}