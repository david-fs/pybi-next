export type TAppEventService = ReturnType<typeof getServices>;

export function getServices() {
  const resetFilterFns: (() => void)[] = [];

  function onResetFilters(callback: () => void) {
    resetFilterFns.push(callback);
  }

  function resetFilters() {
    resetFilterFns.forEach((fn) => fn());
  }

  return {
    onResetFilters,
    resetFilters,
  };
}
