import { TCpId } from "@/models/types";
import { readonly, ref, Ref } from "vue";
import { getBlacklistFilterFn } from "./filterBlacklist";
import { TComponentServices } from "@/services/componentServices";


export function* iterFilterExpr(update2FilterMap: Map<string, Ref<string>>,
    requestorId: string,
    exceptIds: TCpId[],
    cpServices: TComponentServices
) {

    const exceptIdsSet = new Set<TCpId>([...exceptIds, requestorId])

    const blacklistFn = getBlacklistFilterFn(requestorId, cpServices)

    for (const [updateId, expression] of update2FilterMap.entries()) {

        if (!exceptIdsSet.has(updateId) && !blacklistFn(updateId)) {
            if (expression.value !== '') {
                yield readonly(expression)
            }

        }

    }

}