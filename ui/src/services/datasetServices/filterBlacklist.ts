import { ComponentTag } from "@/models/component"
import { TComponentServices } from "@/services/componentServices";


const mBlacklist = new Map<string, Set<string>>([
    ['elSlicer', new Set([ComponentTag.EChart, 'elTable', 'qsTable'])],
    ['qsSlicer', new Set([ComponentTag.EChart, 'elTable', 'qsTable'])],
])


const alwaysFalseFn = (id: string) => {
    return false
}

export function getBlacklistFilterFn(requestorId: string, cpServices: TComponentServices) {
    const getComponentByUpdateId = cpServices.getComponentByUpdateId

    if (requestorId) {

        const requestorCp = getComponentByUpdateId(requestorId)
        const banSet = mBlacklist.get(requestorCp.tag)

        if (banSet) {
            return (id: string) => {
                return banSet.has(getComponentByUpdateId(id).tag)
            }
        }

        return alwaysFalseFn
    }

    return alwaysFalseFn
}