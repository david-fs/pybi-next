import { InjectionKey } from "vue";
import { TComponentServices } from "./componentServices";
import { TDatasetServices } from "./datasetServices";
import { TSqlAnalyzeService } from "./sqlAnalyzeServices";
import { TDbServices } from "./dbServices";
import { TUtilsService } from "./utilsServices";
import { TReactdataServices } from "./reactdataServices";
import { TWebResourcesService } from "./webResourceServices";
import { TAppMetaInfos } from "@/models/types";
import { TActionsService } from "./actionsServices";
import { TAppEventService } from "./appEventServices";
import { TComponentInfosService } from "./componentInfosServices";
import { TImgService } from "./imgServices";

export const sqlAnalyzeKey =
  "sqlAnalyzeKey" as unknown as InjectionKey<TSqlAnalyzeService>;
export const componentKey =
  "componentKey" as unknown as InjectionKey<TComponentServices>;
export const datasetKey =
  "datasetKey" as unknown as InjectionKey<TDatasetServices>;
export const dbKey = "dbKey" as unknown as InjectionKey<TDbServices>;
export const utilsKey = "utilsKey" as unknown as InjectionKey<TUtilsService>;
export const reactdataServicesKey =
  "reactdataServicesKey" as unknown as InjectionKey<TReactdataServices>;
export const dynamicComponentKey = "dynamicComponentKey";
export const metaInfoKey =
  "metaInfoKey" as unknown as InjectionKey<TAppMetaInfos>;
export const webResourcesServiceKey =
  "webResourcesServiceKey" as unknown as InjectionKey<TWebResourcesService>;
export const actionsServiceKey =
  "actionsServiceKey" as unknown as InjectionKey<TActionsService>;
export const appEventServiceKey =
  "appEventServiceKey" as unknown as InjectionKey<TAppEventService>;
export const componentInfosServiceKey =
  "componentInfosServiceKey" as unknown as InjectionKey<TComponentInfosService>;
export const imgServiceKey =
  "imgServiceKey" as unknown as InjectionKey<TImgService>;
