import sysApp from "./main-app";
import testingData from "@/tempData/configFromPy.json";
import elementCps from "../src-element/main";
import quasarCps from "../src-quasar/main";
import echartsCps from "../src-echarts/main";
import mermaidCps from "../src-mermaid/main";
import '@quasar/extras/material-icons/material-icons.css'
import 'quasar/dist/quasar.css'
import { Quasar, Notify } from "quasar";
import langZh from "quasar/lang/zh-CN"

import svg_symbols_temp_data from "@/tempData/svg_symbols.json";

function createSvgSymbols() {

    const svgNs = 'http://www.w3.org/2000/svg'
    const svgEle = document.createElementNS(svgNs, 'svg')
    svgEle.style.height = '0px'
    svgEle.style.width = '0px'
    svgEle.style.display = 'block'

    svg_symbols_temp_data.forEach(svg => {
        const symbolEle = document.createElementNS(svgNs, 'symbol')
        symbolEle.setAttribute('id', svg.id)
        symbolEle.setAttribute('viewBox', '0 0 24 24')
        symbolEle.innerHTML = svg.svg
        svgEle.appendChild(symbolEle)
    })


    document.body.append(svgEle)
}

createSvgSymbols()


const app = sysApp.initApp(testingData as any)




elementCps.forEach(({ tag, cp }) => {
    app.component(tag, cp)
})

quasarCps.forEach(({ tag, cp }) => {
    app.component(tag, cp)
})

mermaidCps.forEach(({ tag, cp }) => {
    console.log(tag, cp);

    app.component(tag, cp)
})

echartsCps.forEach(({ tag, cp }) => {
    console.log(tag, cp);

    app.component(tag, cp)
})


app.use(Quasar, {
    lang: langZh,
    plugins: {
        Notify
    },
    config: {
        notify: {}
    }
}).mount('#app')