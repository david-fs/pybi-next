

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path";



// https://vitejs.dev/config/
export default defineConfig({


    plugins: [
        vue(),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

    define: {
        'process.env': {}
    },

    build: {
        sourcemap: false,
        // target: 'esnext',
        // assetsInlineLimit: 488280,
        cssCodeSplit: false,

        outDir: 'dist/app-libs',
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: path.resolve(__dirname, 'src/main-app.ts'),
            fileName: 'sysApp'
        },

        rollupOptions: {
            external: ['vue'],
            output: [{
                // file: '../pyvisflow/template/bundle.js',
                format: 'iife',
                name: 'sysApp',
                globals: {
                    vue: "Vue"
                }
            }]
        }
    }




})
