

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path";


// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),

    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

    define: {
        'process.env': {}
    },

    publicDir: false,

    build: {
        cssCodeSplit: false,

        outDir: 'dist/mermaid-libs',
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: path.resolve(__dirname, 'src-mermaid/main.ts'),
            fileName: 'mermaidCps'
        },

        rollupOptions: {
            external: ['vue'],
            output: [{
                format: 'iife',
                name: 'mermaidCps',
                globals: {
                    vue: "Vue"
                }
            }]
        }
    }




})
