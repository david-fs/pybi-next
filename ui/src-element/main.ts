import Slicer from "./components/Slicer.vue";
import Table from "./components/Table.vue";
import Upload from "./components/Upload.vue";
import Tabs from "./components/Tabs.vue";
import Affix from "./components/Affix.vue";
import Input from "./components/Input.vue";
import NumberSlider from "./components/NumberSlider.vue";
import Sizebar from "./components/Sizebar.vue";


export default [
    { tag: 'elSlicer', cp: Slicer },
    { tag: 'elTable', cp: Table },
    { tag: 'elUpload', cp: Upload },
    { tag: 'elTabs', cp: Tabs },
    { tag: 'Affix', cp: Affix },
    { tag: 'elInput', cp: Input },
    { tag: 'elNumberSlider', cp: NumberSlider },
    { tag: 'elSizebar', cp: Sizebar },
]