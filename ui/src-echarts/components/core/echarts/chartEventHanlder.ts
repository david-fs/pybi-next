import { TEchartUpdateInfo, TEChartType } from "@/models/reactiveComponent";
import { TEchartEventParams } from "./types";


type TParamValueGetter = (eventParams: TEchartEventParams, info: TEchartUpdateInfo) => any

function barParamValueGetter(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {

    if (info.valueType.slice(0, 1) === ':') {
        const propName = info.valueType.slice(1)
        return eventParams[propName] as string
    }

    if (info.valueType === 'x' || info.valueType === 'y') {
        return eventParams['name'] as string
    }

    if (info.valueType === 'color') {
        return eventParams['seriesName'] as string
    }

    throw new Error(`bar chart not support value type : ${info.valueType}`);
}

function lineParamValueGetter(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {
    if (info.valueType.slice(0, 1) === ':') {
        const propName = info.valueType.slice(1)
        return eventParams[propName] as string
    }

    if (info.valueType === 'x' || info.valueType === 'y') {
        return eventParams['name'] as string
    }

    if (info.valueType === 'color') {
        return eventParams['seriesName'] as string
    }

    throw new Error(`line chart not support value type : ${info.valueType}`);
}

function pieParamValueGetter(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {
    if (info.valueType.slice(0, 1) === ':') {
        const propName = info.valueType.slice(1)
        return eventParams[propName] as string
    }

    if (info.valueType === 'name') {
        return eventParams.name
    }

    throw new Error(`pie chart not support value type : ${info.valueType}`);
}

function scatterParamValueGetter(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {
    if (info.valueType.slice(0, 1) === ':') {
        const propName = info.valueType.slice(1)
        return eventParams[propName] as string
    }

    if (info.valueType === 'x') {
        return eventParams.data[0] as string
    }

    if (info.valueType === 'y') {
        return eventParams.data[1] as string
    }

    if (info.valueType === 'color') {
        return eventParams['seriesName'] as string
    }


    throw new Error(`scatter chart not support value type : ${info.valueType}`);
}

function mapParamValueGetter(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {
    if (info.valueType === 'name') {
        return eventParams['name'] as string
    }

    throw new Error(`scatter chart not support value type : ${info.valueType}`);
}

function radarParamValueGetter(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {
    if (info.valueType.slice(0, 1) === ':') {
        const propName = info.valueType.slice(1)
        return eventParams[propName] as string
    }

    if (info.valueType === 'name') {
        return eventParams.name
    }

    throw new Error(`radar chart not support value type : ${info.valueType}`);
}



const m_typeMap = new Map<TEChartType, TParamValueGetter>(
    [
        ['bar', barParamValueGetter],
        ['line', lineParamValueGetter],
        ['pie', pieParamValueGetter],
        ['scatter', scatterParamValueGetter],
        ['map', mapParamValueGetter],
        ['radar', radarParamValueGetter]
    ]
)



export function getParamValue(eventParams: TEchartEventParams, info: TEchartUpdateInfo) {
    const chartType = eventParams.seriesType as TEChartType

    if (m_typeMap.has(chartType)) {
        return m_typeMap.get(chartType)!(eventParams, info)
    }

    throw new Error(`ParamValueGetter not support chart type:[${chartType}]`);

}