import { TOptionsOperator, TSeriesOperator } from "./utils";


export function fixOptions(optOperator: TOptionsOperator, seriesOperator: TSeriesOperator) {

    if (seriesOperator.seriesHasData()) {
        const itemData = seriesOperator.series['data'][0] as any[]
        if (itemData.length > 0) {

            if (typeof itemData[0] === 'number') {
                optOperator.setProps(`xAxis[${seriesOperator.getXAxisIndex()}].type`, 'value')
            }

        }
    }
}