import { TOptionsOperator, TSeriesOperator } from "./utils";


export function fixOptions(optOperator: TOptionsOperator, seriesOperator: TSeriesOperator) {

    if (seriesOperator.seriesHasData()) {
        const itemData = seriesOperator.series['data'][0] as any[]
        if (itemData.length > 0) {


            if (typeof itemData[0] === 'string') {
                optOperator.setProps(`xAxis[${seriesOperator.getXAxisIndex()}].type`, 'category')
            }

            if (typeof itemData[1] === 'string') {
                optOperator.setProps(`yAxis[${seriesOperator.getYAxisIndex()}].type`, 'category')
            }
        }
    }
}