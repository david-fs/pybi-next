import { TEchartUpdateInfo } from "@/models/reactiveComponent";
import { TDatasetServices } from "@/services/datasetServices";
import { TDbServices } from "@/services/dbServices";
import { TSqlAnalyzeService } from "@/services/sqlAnalyzeServices";
import { TUtilsService } from "@/services/utilsServices";
import { Ref } from "vue";

export type TPerformType = Ref<'forward' | 'back' | 'none'>


export type TServices = {
    dataset: TDatasetServices,
    sqlAnalyze: TSqlAnalyzeService,
    db: TDbServices,
    utils: TUtilsService
}

export type TEchartEventParams = echarts.ECElementEvent



export type TEchartInstance = echarts.EChartsType


export type TGetValueFromParams = (eventParams: TEchartEventParams, info: TEchartUpdateInfo) => string