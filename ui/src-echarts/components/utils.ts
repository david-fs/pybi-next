import { EChart, TUpdateInfoActionType } from "@/models/reactiveComponent";
import { computed } from "vue";
import { TDatasetServices } from "@/services/datasetServices";
import { TSqlAnalyzeService } from "@/services/sqlAnalyzeServices";
import { TDbServices } from "@/services/dbServices";
import { createController } from "./core/echarts";
import { TEchartEventParams } from "./core/echarts/types";
import { TUtilsService } from "@/services/utilsServices";


export function getEChartsFilterUtils(model: EChart, services: {
    dataset: TDatasetServices,
    sqlAnalyze: TSqlAnalyzeService,
    db: TDbServices,
    utils: TUtilsService
}) {

    const chartController = createController(model, services)

    function getData() {
        return computed(() => chartController.curController.value.useOptions())
    }


    function removeFilter() {

        chartController.curController.value.removeFilter()

    }

    function addFilter(eventParams: TEchartEventParams, actionType: TUpdateInfoActionType) {
        return chartController.curController.value.addFilter(eventParams, actionType)
    }


    return {
        getData,
        addFilter,
        removeFilter,
        chartController,
    }
}




